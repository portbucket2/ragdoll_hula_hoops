﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamPivot : MonoBehaviour
{
    public Animator anim;
    public GameManager gm;

    private void Start()
    {
        anim = GetComponent<Animator>();
        gm = FindObjectOfType<GameManager>();
        gm.OnLevelComplete += LevelComplete;
    }

    void LevelComplete(bool value)
    {
        anim.enabled = true;
    }
}
