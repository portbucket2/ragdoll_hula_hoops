﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollowScene2 : MonoBehaviour
{
    public StandingHoopFall milkCrateChallenge;
    public Transform followTarget;
    public float speed;
    private Vector3 offset;
    public float finalHeight;
    private float finalHeightCurrent = 0;

    private void Start()
    {
        offset = transform.position - followTarget.position;

        milkCrateChallenge.OnRagdollStart += RagdollStart;
    }

    private void RagdollStart(bool b)
    {
        finalHeightCurrent = finalHeight;
        Debug.Log(finalHeightCurrent);
    }


    private void FixedUpdate()
    {
        Vector3 target = Vector3.up * finalHeightCurrent;

        transform.position = Vector3.Lerp(transform.position, followTarget.position + offset + target, Time.fixedDeltaTime * speed);
    }
}
