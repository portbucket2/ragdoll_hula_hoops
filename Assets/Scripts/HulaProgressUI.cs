﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HulaProgressUI : MonoBehaviour
{
    public event Action<float> OnHulaProgressAnimationDone;
    private bool done = false;

    public TMP_Text text;
    public Image progress;
    public float speed;
    public float end = 0;
    public float progressValue = 0;
    public bool noMoreToUnlock = false;

    private void Awake()
    {
        //enabled = false;
    }

    public void Initiate()
    {
        done = false;
    }

    void Update()
    {
        if (noMoreToUnlock)
        {
            progress.fillAmount = 1;
            text.SetText("100%");
            enabled = false;
            return;
        }
        if (Stats.userData.nextUnlockableHulaIndex <= 10 && progressValue <= end)
        {
            progressValue += Time.deltaTime * speed;
            progress.fillAmount = progressValue;
            string s = (progress.fillAmount * 100).ToString("F0") + "%";
            text.SetText(s);
        }
        else
        {
            if (!done)
            {
                if (Stats.userData.nextUnlockableHulaIndex <= 10)
                {
                    GetComponent<Animator>().Play("HulaProgressUIPop");
                    OnHulaProgressAnimationDone?.Invoke(end);
                }
                else
                {
                    progress.fillAmount = 1;
                }
                done = true;
            }
        }
    }

    
}
