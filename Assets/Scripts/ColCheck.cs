﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColCheck : MonoBehaviour
{
    public Sandbox sandbox;
    public Sandbox sandbox2;

    public int type;

    private void OnMouseEnter()
    {
        sandbox.dragging[type] = true;
        if (sandbox2 != null)
        {
            sandbox2.dragging[type] = true;
        }
    }
    private void OnMouseExit()
    {
        sandbox.dragging[type] = false;
        if (sandbox2 != null)
        {
            sandbox2.dragging[type] = false;
        }
    }
}
