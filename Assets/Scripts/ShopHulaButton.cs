﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShopHulaButton : MonoBehaviour
{
    public GameObject[] hulaIcons;

    public GameObject buy;
    public GameObject active;
    public GameObject locked;
    public TMP_Text priceText;

    public Text t;
    public GameObject claim;
    public int hulaIndex;

    private Button btn;

    public event Action<int> OnClickActionEvent;

    public void Initialize()
    {
        hulaIcons = Stats.reorganizeHula(hulaIcons);

        btn = GetComponent<Button>();
        t.text = "Hula " + hulaIndex;
        priceText.SetText(Stats.userData.hula[hulaIndex].price.ToString());

        if (Stats.userData.hula[hulaIndex].unlockType == UnlockType.coin)
        {
            if (Stats.userData.hula[hulaIndex].claimed == 0)
            {
                buy.SetActive(true);
            }
            else
            {
                buy.SetActive(false);
            }
        }
        else
        {
            if (Stats.userData.hula[hulaIndex].progress < 100)
            {
                Lock();
            }
            else if (Stats.userData.hula[hulaIndex].claimed == 0)
            {
                claim.SetActive(true);
            }
        }

        hulaIcons[hulaIndex].SetActive(true);
    }

    public void Equip()
    {
        active.SetActive(true);
    }
    public void Unequip()
    {
        active.SetActive(false);
    }
    public void Lock()
    {
        //btn.interactable = false;
        locked.SetActive(true);
    }
    public void Unlock()
    {
        //btn.interactable = true;
        locked.SetActive(false);
    }

    public void Bought()
    {
        buy.SetActive(false);
    }

    public void Claimed()
    {
        claim.SetActive(false);
    }

    public void OnClick()
    {
        //if (Stats.userData.hula[hulaIndex].progress >= 100)
        //{
        //    claim.SetActive(false);
        //}

        //if (Stats.userData.hula[hulaIndex].unlockType == UnlockType.coin)
        //{
        //    if (Stats.userData.hula[hulaIndex].claimed == 0 && Stats.userData.coin >= Stats.userData.hula[hulaIndex].price)
        //    {
        //        buy.SetActive(false);
        //    }
        //}

        OnClickActionEvent?.Invoke(hulaIndex);
    }
}
