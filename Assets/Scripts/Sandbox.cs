﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sandbox : MonoBehaviour
{
    public GameObject[] hulaC;
    public GameObject[] hulaL;
    public GameObject[] hulaR;

    private int currentHulaIndex;

    [SerializeField] private float pivot1WaistMovement = 0.075f;
    [SerializeField] private float pivot1RhandMovement = 0.2f;
    [SerializeField] private float pivot1LhandMovement = 0.2f;
    [SerializeField] private float hoopDownMin = 0f;
    [SerializeField] private float rootValue = 1f;
    public int totalHoops = 0;
    internal int hoopsDone = 0;

    public GameObject imageRHand;
    public GameObject imageWaist;
    public GameObject imageLHand;

    public Image circleWaist;
    public Image circleRHand;
    public Image circleLHand;

    private bool HoopGirlComplete = false;

    public float sliderAccceleration = 1f;
    public float sliderDeceleration = 1f;

    public Slider sliderWaist;
    public Slider sliderRHand;
    public Slider sliderLHand;

    public bool[] dragging = {false, false, false};

    public Text debugText;

    public GameObject pivot1;
    public GameObject pivot1RHand;
    public GameObject pivot1LHand;

    public Animator pivot2Animator;
    public Animator blobmanAnimator;

    public float acceleration = 1.2f;
    public float deceleration = 0.5f;

    public float momentum_max = 2.25f;

    public float wiggleDeceleration = 0.75f;

    private Vector2 lastMousePos = Vector2.zero;

    private GameManager gm;

    public WiggleControl waistWiggle;
    public WiggleControl rHandWiggle;
    public WiggleControl lHandWiggle;

    private bool interactable = true;

    private void Start()
    {
        if (Stats.userData == null)
        {
            Stats.userData = SaveSystem.Load();
        }

        hulaC = Stats.reorganizeHula(hulaC);
        hulaL = Stats.reorganizeHula(hulaL);
        hulaR = Stats.reorganizeHula(hulaR);

        int waistBlend = Animator.StringToHash("waistBlend");
        int waistBlendZ = Animator.StringToHash("waistBlendZ");
        int rHandBlend = Animator.StringToHash("rHandBlend");
        int rHandBlendZ = Animator.StringToHash("rHandBlendZ");
        int lHandBlend = Animator.StringToHash("lHandBlend");
        int lHandBlendZ = Animator.StringToHash("lHandBlendZ");
        int upDownBlendHash = Animator.StringToHash("UpDownBlend");

        waistWiggle.Init(
            sandboxReference: this,
            animHash_Xpos: waistBlend,
            animHash_Zpos: waistBlendZ,
            upDownBlendHash: upDownBlendHash
            );
        rHandWiggle.Init(
            sandboxReference: this,
            animHash_Xpos: rHandBlend,
            animHash_Zpos: rHandBlendZ,
            upDownBlendHash: upDownBlendHash
            );
        lHandWiggle.Init(
            sandboxReference: this,
            animHash_Xpos: lHandBlend,
            animHash_Zpos: lHandBlendZ,
            upDownBlendHash: upDownBlendHash
            );

        waistWiggle.hoopTiltMinimum = hoopDownMin;
        waistWiggle.hoopFollowsLimb = pivot1WaistMovement;
        rHandWiggle.hoopFollowsLimb = pivot1RhandMovement;
        lHandWiggle.hoopFollowsLimb = pivot1LhandMovement;

        gm = FindObjectOfType<GameManager>();

        blobmanAnimator.SetFloat("Root", rootValue);

        gm.totalHoopsNeeded += totalHoops;

        gm.OnEquipHula += EquipHula;
        gm.OnToggleInteractability += ToggleInteractability;

        currentHulaIndex = Stats.userData.equippedHula;

        EquipHula(currentHulaIndex);
    }

    void ToggleInteractability(bool value)
    {
        interactable = value;
    }

    void EquipHula(int hulaIndex)
    {
        hulaC[currentHulaIndex].SetActive(false);
        hulaL[currentHulaIndex].SetActive(false);
        hulaR[currentHulaIndex].SetActive(false);

        hulaC[hulaIndex].SetActive(true);
        hulaL[hulaIndex].SetActive(true);
        hulaR[hulaIndex].SetActive(true);

        currentHulaIndex = hulaIndex;
    }

    private void FixedUpdate()
    {
        if (!interactable)
        {
            return;
        }
        Vector2 relative = (Vector2)Input.mousePosition - lastMousePos;
        waistWiggle.ManageMotion(relative.x, dragging[0]);
        rHandWiggle.ManageMotion(relative.y, dragging[1]);
        lHandWiggle.ManageMotion(relative.y, dragging[2]);


        if (hoopsDone >= totalHoops)
        {
            if (!HoopGirlComplete)
            {
                HoopGirlComplete = true;
                gm.HoopDone();
                //gm.StopAnim();
                //particles.Play();
                //particles1.Play();
                //levelCompleteUI.SetActive(true);
            }
        }

        lastMousePos = Input.mousePosition;
    }


}
