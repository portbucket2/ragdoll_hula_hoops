﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using com.faithstudio.SDK;
using GameAnalyticsSDK;
using TMPro;
using Dev_Nirjhor;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public GameObject lockIcon;

    public GameObject coinsVisual;

    public Animator shopHoopIconsAnimator;

    public GameObject[] unlockedItemIcon;

    public GameObject claimHulaButtonRoot;
    public GameObject claimHulaButton;
    public GameObject claimHulaButtonFTUE;

    private Vector2 shopHulaButtonsPosCurrent = Vector2.zero;
    private Vector2 shopHulaButtonsPosTarget = Vector2.zero;
    public GameObject shopHulaButtonsRoot;
    public float shopHulaButtonsScrollSpeed;
    private int currentScrollPage = 0;
    public List<GameObject> dotsActive;
    public List<GameObject> dotsInactive;

    public GameObject[] shopHulaIcons;

    public HulaProgressUI hulaProgressUI;

    public event Action<int> OnEquipHula;
    public event Action<bool> OnToggleInteractability;

    public GameObject shop;

    public List<ShopHulaButton> shopHulaButtons;
    public Text EquippedHula;
    private int currentHulaIndex = 0;

    public TMP_Text haveCoinsText;
    public GameObject coinSystem;
    public GameObject coinMultiplyIndicator;
    public Text coinMultiplyText;
    public float coinMultiplyIndicatorRange;
    public float coinMultiplyIndicatorSpeed;
    private float coinMultiplyIndicatorPhase = 0;
    private bool stopMultiplier = false;

    private float coinMultiplyValue = 1f;

    public GameObject levelEndScreen;
    public Image hulaProgress;
    public Text coinsText;
    public GameObject hulaUnlocked;

    private bool levelComplete = false;
    public bool tutDone = false;
    public Animator animator;
    public GameObject finger;
    public GameObject levelCompleteUI;
    public ParticleSystem particles;
    public ParticleSystem particles1;

    public int totalHoopsNeeded = 0;
    public int totalHoopsDone = 0;

    private bool clickedRewarded = false;

    public event Action<bool> OnLevelComplete;

    public void HoopDone()
    {
        if (levelComplete)
        {
            return;
        }
        if (totalHoopsDone >= totalHoopsNeeded)
        {
            particles.Play();
            particles1.Play();
            animator.Play("LevelEndUIEntry");
            levelComplete = true;
            FacebookAnalyticsManager.Instance.FBALevelComplete(Stats.currentFakeLevel);
            AdjustSDKManager.Instance.InvokePreConfigEvent(AdjustEventTrack.LEVEL_COMPLETE);
            //GameAnalytics.NewDesignEvent("Level_Complete", Stats.currentFakeLevel);
            //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, Stats.currentFakeLevel.ToString());
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "World01", "Level " + Stats.currentFakeLevel);
            OnLevelComplete?.Invoke(true);
        }
    }

    private void Awake()
    {
        if (Stats.userData == null)
        {
            Stats.userData = SaveSystem.Load();
        }

        Instance = this;
        Debug.Log("Current Fake level " + Stats.currentFakeLevel.ToString());
        PlayerPrefs.SetInt("CurrentFakeLevel", Stats.currentFakeLevel);

        for (int i = 0; i < shopHulaButtons.Count; i++)
        {
            shopHulaButtons[i].hulaIndex = i;
            shopHulaButtons[i].OnClickActionEvent += HulaButtonPressed;
            shopHulaButtons[i].Initialize();
        }
    }

    void HulaButtonPressed(int hulaIndex)
    {
        //EquippedHula.text = "Equipped Hula " + hulaIndex;

        bool justClaimed = false;

        if (Stats.userData.hula[hulaIndex].unlockType == UnlockType.coin)
        {
            if (Stats.userData.hula[hulaIndex].claimed == 0 && Stats.userData.coin >= Stats.userData.hula[hulaIndex].price)
            {
                //PURCHASING HULA WITH COINS

                justClaimed = true;

                Stats.userData.coin -= Stats.userData.hula[hulaIndex].price;
                haveCoinsText.text = Stats.userData.coin.ToString();
                Stats.userData.equippedHula = hulaIndex;
            }
        }
        else
        {
            if (Stats.userData.hula[hulaIndex].progress >= 100 && Stats.userData.hula[hulaIndex].claimed == 0)
            {
                //CLAIMING AN UNCLAIMED HULA

                //justClaimed = true;

                SDKManager.ShowRewardedVideo(true, Stats.currentFakeLevel,
                    OnClicked: (x) =>
                    {
                        clickedRewarded = true;
                    },

                    OnFailedToDisplay: (x, y) =>
                    {
                        clickedRewarded = false;
                    },

                    OnReceivedReward: (x, y) =>
                    {
                        GetRewardedHula(hulaIndex);
                    },

                    OnHidden: (x) =>
                    {
                        clickedRewarded = false;
                    });
            }
        }

        if (justClaimed)
        {
            Stats.userData.hula[hulaIndex].claimed = 1;
            shopHoopIconsAnimator.Play("ClaimInShopDance", -1, 0);
            shopHulaButtons[hulaIndex].Bought();
        }

        if (Stats.userData.hula[hulaIndex].claimed == 1)
        {
            Stats.userData.equippedHula = hulaIndex;
            OnEquipHula?.Invoke(hulaIndex);
            lockIcon.SetActive(false);
        }
        else
        {
            lockIcon.SetActive(true);
        }

        shopHulaButtons[currentHulaIndex].Unequip();
        shopHulaButtons[hulaIndex].Equip();

        shopHulaIcons[currentHulaIndex].SetActive(false);
        shopHulaIcons[hulaIndex].SetActive(true);
        
        currentHulaIndex = hulaIndex;
    }

    void GetRewardedHula(int hulaIndex)
    {
        Stats.userData.hula[hulaIndex].claimed = 1;
        shopHoopIconsAnimator.Play("ClaimInShopDance");
        shopHulaButtons[hulaIndex].Claimed();

        Stats.userData.equippedHula = hulaIndex;
        OnEquipHula?.Invoke(hulaIndex);
        lockIcon.SetActive(false);
     
        SaveSystem.Save(Stats.userData);
    }

    private void Start()
    {
        unlockedItemIcon = Stats.reorganizeHula(unlockedItemIcon);
        shopHulaIcons = Stats.reorganizeHula(shopHulaIcons);

        FacebookAnalyticsManager.Instance.FBALevelStart(Stats.currentFakeLevel);
        AdjustSDKManager.Instance.InvokePreConfigEvent(AdjustEventTrack.LEVEL_START);
        //GameAnalytics.NewDesignEvent("Level_Start", Stats.currentFakeLevel);
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, Stats.currentFakeLevel.ToString());
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "World01", "Level " + Stats.currentFakeLevel);

        animator = GetComponent<Animator>();

        int i = Stats.currentFakeLevel;
        if (i == 1)
        {
            finger.SetActive(true);
            animator.Play("Tutorial");
        }
        if (i == 2)
        {
            finger.SetActive(true);
            animator.Play("Tutorial2");
        }

        EquippedHula.text = "Equipped Hula " + Stats.userData.equippedHula;

        haveCoinsText.text = Stats.userData.coin.ToString();

        currentHulaIndex = Stats.userData.equippedHula;

        shopHulaButtons[currentHulaIndex].Equip();
        shopHulaIcons[currentHulaIndex].SetActive(true);

        hulaProgressUI.OnHulaProgressAnimationDone += HulaProgressAnimationDone;
    }

    private void Update()
    {
        shopHulaButtonsPosCurrent.x = Mathf.Lerp(shopHulaButtonsPosCurrent.x, shopHulaButtonsPosTarget.x, Time.deltaTime * shopHulaButtonsScrollSpeed);
        shopHulaButtonsPosCurrent.y = shopHulaButtonsRoot.transform.localPosition.y;
        shopHulaButtonsRoot.transform.localPosition = shopHulaButtonsPosCurrent;

        if (stopMultiplier)
        {
            return;
        }

        coinMultiplyIndicatorPhase += Time.deltaTime * coinMultiplyIndicatorSpeed;

        Vector2 pos = coinMultiplyIndicator.transform.localPosition;
        pos.x = Mathf.PingPong(coinMultiplyIndicatorPhase, coinMultiplyIndicatorRange) - coinMultiplyIndicatorRange/2;
        coinMultiplyIndicator.transform.localPosition = pos;


        int currentTrueLevel = Stats.CalculateNextLevel(0);
        //int coinsToAdd = Stats.levelSpecificCoin[currentTrueLevel];
        int coinsToAdd = Stats.allLevelCoin;

        float _5xRange = coinMultiplyIndicatorRange / 14;
        float _4xRange = coinMultiplyIndicatorRange / 7 + _5xRange;
        float _3xRange = coinMultiplyIndicatorRange / 7 + _4xRange;

        if (Mathf.Abs(pos.x) < _5xRange)
        {
            coinMultiplyValue = 5f;
            UpdateCoinMultiplyText(coinsToAdd);
        }
        else if (Mathf.Abs(pos.x) < _4xRange)
        {
            coinMultiplyValue = 4f;
            UpdateCoinMultiplyText(coinsToAdd);
        }
        else if (Mathf.Abs(pos.x) < _3xRange)
        {
            coinMultiplyValue = 3f;
            UpdateCoinMultiplyText(coinsToAdd);
        }
        else
        {
            coinMultiplyValue = 2f;
            UpdateCoinMultiplyText(coinsToAdd);
        }
    }

    public void ScrollRight()
    {
        if (shopHulaButtonsPosTarget.x <= -1180)
        {
            return;
        }
        
        dotsInactive[currentScrollPage].SetActive(true);
        dotsActive[currentScrollPage].SetActive(false);
        currentScrollPage++;
        dotsInactive[currentScrollPage].SetActive(false);
        dotsActive[currentScrollPage].SetActive(true);
        
        shopHulaButtonsPosTarget.x -= 1185;
    }

    public void ScrollLeft()
    {
        if (shopHulaButtonsPosTarget.x >= -5)
        {
            return;
        }
        
        dotsInactive[currentScrollPage].SetActive(true);
        dotsActive[currentScrollPage].SetActive(false);
        currentScrollPage--;
        dotsInactive[currentScrollPage].SetActive(false);
        dotsActive[currentScrollPage].SetActive(true);

        shopHulaButtonsPosTarget.x += 1185;
    }


    void UpdateCoinMultiplyText(int coinsToAdd)
    {
        coinMultiplyText.text = "+" + coinMultiplyValue * coinsToAdd;
    }

    public void MultiPlyCoin()
    {
        stopMultiplier = true;

        SDKManager.ShowRewardedVideo(true, Stats.currentFakeLevel,
            OnClicked: (x) =>
            {
                clickedRewarded = true;
            },

            OnFailedToDisplay: (x, y) =>
            {
                clickedRewarded = false;
                stopMultiplier = false;
                GetDefaultCoin();
            },

            OnReceivedReward: (x, y) =>
            {
                GotRewardedCoin();
            },

            OnHidden: (x) => 
            {
                clickedRewarded = false;
                NextLevel();
            });

        haveCoinsText.text = Stats.userData.coin.ToString();
    }

    void GotRewardedCoin()
    {
        int currentTrueLevel = Stats.CalculateNextLevel(0);

        int coinsToAdd = Stats.levelSpecificCoin[currentTrueLevel];

        Stats.userData.coin -= coinsToAdd;

        float multipliedCoin = coinsToAdd * coinMultiplyValue;

        Stats.userData.coin += (int)multipliedCoin; SaveSystem.Save(Stats.userData);
    }

    public void GetDefaultCoin()
    {
        if (stopMultiplier)
        {
            return;
        }
        if (!clickedRewarded)
        {
            SDKManager.ShowInterstitialAd();

            NextLevel();
        }
    }

    public void ClaimHula()
    {
        int hulaIndex = Stats.userData.nextUnlockableHulaIndex;

        if (Stats.userData.nextUnlockableHulaIndex > 1)
        {
            SDKManager.ShowRewardedVideo(true, Stats.currentFakeLevel,
                OnClicked: (x) =>
                {
                    clickedRewarded = true;
                },

                OnFailedToDisplay: (x, y) =>
                {
                    clickedRewarded = false;
                },

                OnReceivedReward: (x, y) =>
                {
                    GetRewardedHula(hulaIndex);
                },

                OnHidden: (x) =>
                {
                    clickedRewarded = false;
                });
        }
        else
        {
            Stats.userData.hula[hulaIndex].claimed = 1;
            Stats.userData.equippedHula = hulaIndex;
        }

        if (Stats.userData.nextUnlockableHulaIndex <= Stats.maxUnlockableHulaIndex)
        {
            Stats.userData.nextUnlockableHulaIndex++;
        }
        //Stats.userData.equippedHula = hulaIndex;

        HulaButtonPressed(hulaIndex);

        //NextLevel();
        FromHulaClaimToCoinSystem();
    }
    public void LoseHula()
    {
        if (Stats.userData.nextUnlockableHulaIndex <= Stats.maxUnlockableHulaIndex)
        {
            Stats.userData.nextUnlockableHulaIndex++;
        }
        //NextLevel();
        FromHulaClaimToCoinSystem();
    }

    void FromHulaClaimToCoinSystem()
    {
        claimHulaButtonRoot.SetActive(false);

        stopMultiplier = false;

        coinSystem.SetActive(true);
    }

    public void ShowShop()
    {
        coinsVisual.SetActive(true);
        shop.SetActive(true);
        OnToggleInteractability?.Invoke(false);
    }

    public void CloseShop()
    {
        coinsVisual.SetActive(false);
        shop.SetActive(false);
        OnToggleInteractability?.Invoke(true);
        SaveSystem.Save(Stats.userData);
    }

    public void StopAnim()
    {
        tutDone = true;
        animator.StopPlayback();
        finger.SetActive(false);
    }

    public void ShowLevelEndScreen()
    {
        coinsVisual.SetActive(true);
        levelEndScreen.SetActive(true);

        int currentTrueLevel = Stats.CalculateNextLevel(0) - 1;
        //Debug.LogError("True " + currentTrueLevel);

        if (Stats.userData.nextUnlockableHulaIndex >= Stats.maxUnlockableHulaIndex)
        {
            hulaProgressUI.noMoreToUnlock = true;
            hulaProgressUI.gameObject.SetActive(true);
            coinsText.text = "Get +" + Stats.allLevelCoin;
            Stats.userData.coin += Stats.allLevelCoin;

            return;
        }

        //int progressToAdd = Stats.levelSpecificProgress[currentTrueLevel] * (int)Stats.progressMultiplier;
        int progressToAdd = Stats.levelSpecificProgress[currentTrueLevel];
        int coinsToAdd = Stats.levelSpecificCoin[currentTrueLevel];

        int currentProgress = Stats.userData.hula[Stats.userData.nextUnlockableHulaIndex].progress;

        int totalProgress = currentProgress + progressToAdd;

        hulaProgressUI.gameObject.SetActive(true);
        hulaProgressUI.progressValue = (float)currentProgress / 100;
        hulaProgressUI.end = (float)totalProgress / 100;

        if (totalProgress >= 100)
        {
            stopMultiplier = true;
            coinSystem.SetActive(false);

            totalProgress = 100;
        }

        //coinsText.text = "Get Coin: " + coinsToAdd + "\nHave Coin: " + Stats.userData.coin;
        coinsText.text = "Get +" + coinsToAdd;

        Stats.userData.coin += coinsToAdd;
        Stats.userData.hula[Stats.userData.nextUnlockableHulaIndex].progress = totalProgress;

        //SaveSystem.Save(Stats.userData);
    }

    public void HulaProgressAnimationDone(float progress)
    {
        hulaProgressUI.enabled = false;
        if (progress >= 1)
        {
            if (Stats.userData.nextUnlockableHulaIndex <= Stats.maxUnlockableHulaIndex)
            {
                hulaProgressUI.gameObject.SetActive(false);
                coinsVisual.SetActive(false);
                hulaUnlocked.SetActive(true);
                unlockedItemIcon[Stats.userData.nextUnlockableHulaIndex].SetActive(true);
            }

            if (Stats.userData.nextUnlockableHulaIndex == 1)
            {
                claimHulaButton.SetActive(false);
                claimHulaButtonFTUE.SetActive(true);
            }
        }
        else
        {
            coinSystem.SetActive(true);
        }
    }

    public void NextLevel()
    {
        //int buildIndex = SceneManager.GetActiveScene().buildIndex + 1;

        //if (buildIndex >= SceneManager.sceneCountInBuildSettings)
        //{
        //    buildIndex = 0;
        //}


        //Stats.currentFakeLevel++;

        //SDKManager.ShowInterstitialAd();

        SaveSystem.Save(Stats.userData);

        SceneManager.LoadScene(Stats.CalculateNextLevel(1));
    }

    public void TestFB()
    {
        FacebookAnalyticsManager.Instance.FBALevelComplete(Stats.currentFakeLevel);
    }
    public void Restart()
    {
        //FacebookAnalyticsManager.Instance.FBALevelRestart(Stats.currentFakeLevel);
        AdjustSDKManager.Instance.InvokePreConfigEvent(AdjustEventTrack.LEVEL_RESTART);
        GameAnalytics.NewDesignEvent("Level_Restart:" + Stats.currentFakeLevel.ToString());

        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Undefined, Stats.currentFakeLevel.ToString());

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //void OnApplicationQuit()
    //{
    //    SaveSystem.Save(Stats.userData);
    //}
}
