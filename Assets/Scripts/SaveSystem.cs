﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class SaveSystem
{

    static string path = Application.persistentDataPath + "/hula";

    public static void Save(UserData userData)
    {
        string json = JsonUtility.ToJson(userData);

        File.WriteAllText(path, json);

        Debug.Log("Saved!");
    }

    public static UserData Load()
    {
        if (File.Exists(path))
        {
            string loadedString = File.ReadAllText(path);

            //Debug.Log(loadedString);

            UserData loadedUserData = JsonUtility.FromJson<UserData>(loadedString);

            return loadedUserData;
        }
        else
        {
            return null;
        }
    }
}
