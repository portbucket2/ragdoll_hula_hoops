﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[System.Serializable]
public class WiggleControl
{
    public bool logEnabled;
    public float hoopFollowsLimb;
    public Image circleImage;
    public GameObject emojiEffectObject;
    public Transform hoopPivot1;
    public Animator hoopGravityTiltAnimator;
    public float hoopTiltMinimum;

    Animator characterAnim;
    bool isFullWiggle;


    float lastWiggleDir;
    [SerializeField] float cumulativeWiggleInput;


    float phase_Anim;
    float pivot1Phase;

    float blendXCurrent;
    float blendZCurrent;

    int animHash_Xpos;
    int animHash_Zpos;
    int upDownBlendHash;

    float wiggleDeceleration;
    float momentum_max;
    float acceleration;
    float sliderAccceleration;
    float sliderDeceleration;

    Sandbox sandboxReference;

    public void Init(Sandbox sandboxReference, int animHash_Xpos, int animHash_Zpos, int upDownBlendHash)
    {
        this.wiggleDeceleration = sandboxReference.wiggleDeceleration;
        this.momentum_max = sandboxReference.momentum_max;
        this.acceleration = sandboxReference.acceleration;
        this.characterAnim = sandboxReference.blobmanAnimator;
        this.animHash_Xpos = animHash_Xpos;
        this.animHash_Zpos = animHash_Zpos;

        this.sliderAccceleration = sandboxReference.sliderAccceleration;
        this.sliderDeceleration = sandboxReference.sliderDeceleration;

        this.upDownBlendHash = upDownBlendHash;

        this.sandboxReference = sandboxReference;
    }

    bool isPlayerInteracting = false;

    float hoopMomentum;
    float limbMomentum;
    public void ManageMotion(float xInput, bool dragging)
    {
        isPlayerInteracting = Input.GetMouseButton(0) && dragging && !isFullWiggle;

        if (!isFullWiggle)
        {
            cumulativeWiggleInput += AddedWiggleOnInput(xInput);

            if (cumulativeWiggleInput > momentum_max)
            {
                cumulativeWiggleInput = Mathf.Lerp(cumulativeWiggleInput, 0, Time.fixedDeltaTime * wiggleDeceleration / 3);
            }
            else
            {
                cumulativeWiggleInput = Mathf.Lerp(cumulativeWiggleInput, 0, Time.fixedDeltaTime * wiggleDeceleration/2);
            }
        }


        hoopMomentum = Mathf.Lerp(hoopMomentum, cumulativeWiggleInput, Time.fixedDeltaTime * acceleration);
        limbMomentum = Mathf.Lerp(limbMomentum, cumulativeWiggleInput, Time.fixedDeltaTime * acceleration * 5);


        ControlLimb(limbMomentum);

        ControlHoop(hoopMomentum);
    }

    const float MIN_MANUAL_MOMENTUM =3.5f;
    [SerializeField] float manualMomentum;
    void ControlLimb(float momentum)
    {
        float phaseIncrement;
        if (isPlayerInteracting)
        {
            float targetPhase = Mathf.Asin(lastWiggleDir);

            if (targetPhase < 0) targetPhase += Mathf.PI * 2;//90 or 270
            float diff = targetPhase - phase_Anim;
            if (diff < 0)
            {
                diff = Mathf.PI * 2 + diff;
            }
            float scheduledIncrement;

            if (diff > Mathf.PI * (345) / 180.0f)
            {

                if (diff < Mathf.PI * (350) / 180.0f)
                {
                    manualMomentum = 0;
                    scheduledIncrement = 0;
                }
                else
                {
                    manualMomentum = 0;
                    scheduledIncrement = Mathf.Lerp(diff, 0, Time.fixedDeltaTime * 20);
                }

            }
            else if (diff < Mathf.PI * 15f / 180.0f)
            {
                manualMomentum = 0;
                scheduledIncrement = Mathf.Lerp(diff, 0, Time.fixedDeltaTime * 20);
            }
            else
            {
                if (manualMomentum < momentum) manualMomentum = momentum;

                float targetManualMomentum = (momentum > MIN_MANUAL_MOMENTUM) ? momentum : MIN_MANUAL_MOMENTUM;

                manualMomentum = Mathf.Lerp(manualMomentum, targetManualMomentum, Time.fixedDeltaTime * 20);
                scheduledIncrement = manualMomentum * 8 * Time.fixedDeltaTime;
            }

            if (scheduledIncrement > diff) phaseIncrement = diff;
            else phaseIncrement = scheduledIncrement;

            
            //float limbMomentum_ManualEquivalent = Mathf.Lerp(diff, 0, Time.fixedDeltaTime * 10);
            //phaseIncrement = limbMomentum_ManualEquivalent;
        }
        else
        {
            manualMomentum = 0;
            phaseIncrement = momentum * 8 * Time.fixedDeltaTime;
        }

        phase_Anim = (phase_Anim + phaseIncrement) % (Mathf.PI * 2);

        SetLimbByPhase(phase_Anim);
    }
    void SetLimbByPhase(float phase_Anim)
    {
        blendXCurrent = Mathf.Lerp(blendXCurrent, Mathf.Sin(phase_Anim), Time.fixedDeltaTime * 20f);
        blendZCurrent = Mathf.Lerp(blendZCurrent, Mathf.Cos(phase_Anim), Time.fixedDeltaTime * 20f);
        characterAnim.SetFloat(animHash_Xpos, blendXCurrent);
        characterAnim.SetFloat(animHash_Zpos, blendZCurrent);
    }
    void ControlHoop(float momentum)
    {
        pivot1Phase += Time.fixedDeltaTime * 1f;
        float offset = Mathf.Sin(pivot1Phase) * Random.Range(0.01f, 0.02f) + Mathf.Sin(pivot1Phase) * Random.Range(0.01f, 0.02f);
        hoopPivot1.localPosition = Vector3.Lerp(hoopPivot1.localPosition, Vector3.up * offset * momentum, Time.fixedDeltaTime * 7f);

        hoopPivot1.localPosition = new Vector3(blendXCurrent * hoopFollowsLimb, hoopPivot1.localPosition.y, blendZCurrent * hoopFollowsLimb);
        hoopPivot1.Rotate(Vector3.up, momentum * 12, Space.Self);

        if (isFullWiggle) return;

        if (hoopGravityTiltAnimator)
        {
            hoopGravityTiltAnimator.SetFloat(upDownBlendHash, Mathf.Max(momentum, hoopTiltMinimum));
        }
        else
        {
            if (momentum < 0.2f)
            {
                float yAngle = Mathf.LerpAngle(hoopPivot1.localEulerAngles.y, 0, Time.fixedDeltaTime * sandboxReference.deceleration);
                hoopPivot1.localEulerAngles = Vector3.up * yAngle;
            }
        }

        if (momentum > momentum_max)
        {
            circleImage.gameObject.SetActive(true);
            GameManager.Instance.StopAnim();
            circleImage.fillAmount += Time.fixedDeltaTime * sliderAccceleration;
        }
        else
        {
            circleImage.fillAmount -= Time.fixedDeltaTime * sliderDeceleration;
        }

        if (circleImage.fillAmount >= 1)
        {
            FinalizeWiggle();
        }

    }
    void FinalizeWiggle()
    {
        sandboxReference.hoopsDone++;
        GameManager.Instance.totalHoopsDone++;
        circleImage.gameObject.SetActive(false);
        emojiEffectObject.SetActive(true);
        emojiEffectObject.GetComponent<Animator>().Play("EmojiPop");

        GameManager.Instance.StopAnim();

        cumulativeWiggleInput = Mathf.Max(3f, cumulativeWiggleInput);
        isFullWiggle = true;
    }

    float AddedWiggleOnInput(float x)
    {
        if (!isPlayerInteracting) return 0;

        if (lastWiggleDir == 0)
        {
            lastWiggleDir = Mathf.Sign(x);
        }

        if (x != 0)
        {
            if (lastWiggleDir != Mathf.Sign(x))
            {
                lastWiggleDir = Mathf.Sign(x);
                return 0.2f;
            }
        }
        return 0;
    }

}

