﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Stats : MonoBehaviour
{
    public static bool resetLevels = false;
    public static bool testing = false;
    public static int levelToTest = 6;

    public static int currentFakeLevel = 1;

    public static int progressMultiplier = 1;

    public static UserData userData;

    private static List<int> reorganizedHulaList = new List<int>() {0, 1, 4, 7, 9, 6, 2, 3, 5, 8, 10};

    public static int maxUnlockableHulaIndex = 6;

    public static List<int> levelSpecificProgress = new List<int>() {35, 40, 25, 40, 30, 30, 30, 20, 30, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20};
    public static int allLevelCoin = 50;
    public static List<int> levelSpecificCoin = new List<int>() { 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50};


    public static GameObject[] reorganizeHula(GameObject[] givenList)
    {
        //return givenList;
        GameObject[] newList = new GameObject[givenList.Length];

        for (int i = 0; i < givenList.Length; i++)
        {
            newList[i] = givenList[reorganizedHulaList[i]];
        }


        return newList;
    }

    public static int CalculateNextLevel(int step)
    {
        currentFakeLevel += step;

        if (testing)
        {
            if (currentFakeLevel >= SceneManager.sceneCountInBuildSettings)
            {
                int levelToLoad = ((levelToTest) % (SceneManager.sceneCountInBuildSettings - 1));

                if (levelToLoad == 0)
                {
                    levelToLoad = SceneManager.sceneCountInBuildSettings - 1;
                }

                return levelToLoad;
            }
            else
            {
                return currentFakeLevel;
            }
        }
        else
        {
            if (currentFakeLevel >= SceneManager.sceneCountInBuildSettings)
            {
                int levelToLoad = ((currentFakeLevel) % (SceneManager.sceneCountInBuildSettings - 1));

                if (levelToLoad == 0)
                {
                    levelToLoad = SceneManager.sceneCountInBuildSettings - 1;
                }

                Debug.Log("Actual Level " + levelToLoad.ToString());

                return levelToLoad;
            }
            else
            {
                return currentFakeLevel;
            }
        }
    }
}
