﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum UnlockType
{
    progress,
    coin
}

public class UserData
{
    public int coin;
    public int equippedHula;
    public int nextUnlockableHulaIndex;
    public List<Hula> hula = new List<Hula>();
}

[System.Serializable]
public class Hula
{
    public UnlockType unlockType;
    public int id;
    public int progress;
    public int claimed;
    public int price;
}
