﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class StandingHoopFall : MonoBehaviour
{
    public PhysicMaterial cratesPhysicsMatBeforeRagdoll;
    public PhysicMaterial cratesPhysicsMatAfterRagdoll;
    public List<BoxCollider> cratesCollider;
    public float cratePhysicsMatChangeDelay;

    public float hulaPhysicsDelay;
    public float animStartDelay;
    public float ragdollStartDelay;
    public float crateMassStart;
    public float crateMassEnd;
    public List<Rigidbody> cratesRb;

    public event Action<bool> OnRagdollStart;

    public Animator hulaAnim;
    public MeshCollider hulaCollider;
    public Rigidbody hulaRb;
    public Vector3 hulaForce;
    public Vector3 hulaTorque;

    Animator animator;
    void Start()
    {
        Invoke("StartAnim", animStartDelay);

        animator = GetComponent<Animator>();

        animator.speed = 0;

        foreach (Rigidbody crateRb in cratesRb)
        {
            crateRb.mass = crateMassStart;
        }

        foreach (BoxCollider boxCollider in cratesCollider)
        {
            boxCollider.material = cratesPhysicsMatBeforeRagdoll;
        }
    }

    void StartAnim()
    {
        animator.speed = 1;
        Invoke("cratePhysicsMatChange", cratePhysicsMatChangeDelay);
        Invoke("RagdollStart", ragdollStartDelay);
        hulaAnim.enabled = true;
    }

    void cratePhysicsMatChange()
    {
        foreach (BoxCollider boxCollider in cratesCollider)
        {
            boxCollider.material = cratesPhysicsMatAfterRagdoll;
        }
    }


    void RagdollStart()
    {
        Invoke("HulaPhysicsStart", hulaPhysicsDelay);

        OnRagdollStart?.Invoke(true);


        foreach (Rigidbody crateRb in cratesRb)
        {
            crateRb.mass = crateMassEnd;
        }
        animator.enabled = false;
    }

    void HulaPhysicsStart()
    {
        hulaAnim.enabled = false;
        hulaCollider.isTrigger = false;
        hulaRb.constraints = RigidbodyConstraints.None;
        hulaRb.AddForce(hulaForce, ForceMode.Impulse);
        hulaRb.AddTorque(hulaTorque, ForceMode.Impulse);
    }
}
