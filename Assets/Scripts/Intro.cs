﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using GameAnalyticsSDK;
using Dev_Nirjhor;

public class Intro : MonoBehaviour
{
    public bool resetLevels;
    public bool resetUserData;
    public GameObject a;

    public bool testing;
    public int testLevel = 1;


    private void Start()
    {
        GameAnalytics.Initialize();

        if (resetLevels)
        {
            Stats.currentFakeLevel = 1;
            PlayerPrefs.SetInt("CurrentFakeLevel", Stats.currentFakeLevel);
        }
        else
        {
            Stats.currentFakeLevel = PlayerPrefs.GetInt("CurrentFakeLevel", 1);
        }

        if (testing)
        {
            Stats.currentFakeLevel = testLevel;
            PlayerPrefs.SetInt("CurrentFakeLevel", Stats.currentFakeLevel);
        }


        UserData hulaData = new UserData();

        //Debug.Log(json);
        if (resetUserData || SaveSystem.Load() == null)
        {

            hulaData.coin = 0;
            hulaData.equippedHula = 0;
            hulaData.nextUnlockableHulaIndex = 1;

            hulaData.hula.Add(new Hula { unlockType = UnlockType.progress, id = 0, progress = 100, claimed = 1, price = 0});
            hulaData.hula.Add(new Hula { unlockType = UnlockType.progress, id = 1, progress = 0, claimed = 0, price = 0 });
            hulaData.hula.Add(new Hula { unlockType = UnlockType.progress, id = 2, progress = 0, claimed = 0, price = 0 });
            hulaData.hula.Add(new Hula { unlockType = UnlockType.progress, id = 3, progress = 0, claimed = 0, price = 0 });
            hulaData.hula.Add(new Hula { unlockType = UnlockType.progress, id = 4, progress = 0, claimed = 0, price = 0 });
            hulaData.hula.Add(new Hula { unlockType = UnlockType.progress, id = 5, progress = 0, claimed = 0, price = 0 });
            hulaData.hula.Add(new Hula { unlockType = UnlockType.coin, id = 6, progress = 0, claimed = 0, price = 300 });
            hulaData.hula.Add(new Hula { unlockType = UnlockType.coin, id = 7, progress = 0, claimed = 0, price = 500 });
            hulaData.hula.Add(new Hula { unlockType = UnlockType.coin, id = 8, progress = 0, claimed = 0, price = 1000 });
            hulaData.hula.Add(new Hula { unlockType = UnlockType.coin, id = 9, progress = 0, claimed = 0, price = 1500 });
            hulaData.hula.Add(new Hula { unlockType = UnlockType.coin, id = 10, progress = 0, claimed = 0, price = 2000 });

            SaveSystem.Save(hulaData);
            Stats.userData = hulaData;
        }


        //Debug.Log("hula id: " + h.hula[0].id + ", Unlock Type: " + h.hula[0].unlockType + " progress: " + h.hula[0].progress + ", claimed: " + h.hula[0].claimed);
        //Debug.Log("hula id: " + h.hula[1].id + ", Unlock Type: " + h.hula[1].unlockType + " progress: " + h.hula[1].progress + ", claimed: " + h.hula[1].claimed);
        //Debug.Log("hula id: " + h.hula[2].id + ", Unlock Type: " + h.hula[2].unlockType + " progress: " + h.hula[2].progress + ", claimed: " + h.hula[2].claimed);
        //Debug.Log("hula id: " + h.hula[3].id + ", Unlock Type: " + h.hula[3].unlockType + " progress: " + h.hula[3].progress + ", claimed: " + h.hula[3].claimed);
        //Debug.Log("hula id: " + h.hula[4].id + ", Unlock Type: " + h.hula[4].unlockType + " progress: " + h.hula[4].progress + ", claimed: " + h.hula[4].claimed);
        //Debug.Log("hula id: " + h.hula[5].id + ", Unlock Type: " + h.hula[5].unlockType + " progress: " + h.hula[5].progress + ", claimed: " + h.hula[5].claimed);

        SDKManager.ShowBannerAd();

        SceneManager.LoadScene(Stats.CalculateNextLevel(0));
    }

    public void Analytics()
    {
        a.SetActive(true);
    }

    //[System.Serializable]
    //public class UserData
    //{
    //    public int coin;
    //    public int gameProgress;
    //    public List<Hula> hula = new List<Hula>();
    //}

    //[System.Serializable]
    //public class Hula
    //{
    //    public int id;
    //    public int progress;
    //    public int claimed;
    //}

}
