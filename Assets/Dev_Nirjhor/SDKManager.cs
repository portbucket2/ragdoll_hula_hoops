﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LionStudios;
using LionStudios.Ads;
using GameAnalyticsSDK;
using com.faithstudio.SDK;

namespace Dev_Nirjhor
{
    public class SDKManager : MonoBehaviour
    {
        #region Debug
        ShowAdRequest rewarded_ShowAdRequest;
        ShowAdRequest interstitial_ShowAdRequest;
        ShowAdRequest banner_ShowAdRequest;

        private void Awake()
        {
            MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) =>
            {
                // Show Mediation Debugger
                MaxSdk.ShowMediationDebugger();
            };
        }

        // Start is called before the first frame update
        void Start()
        {
            InitAllAds_Debug();

            LionStudios.Runtime.Sdks.AppLovin.WhenInitialized(() => ShowBannerAd_Debug());
        }

        public void InitAllAds_Debug()
        {
            Init_BannerAd_Debug();

            Init_InterstitialAd_Debug();

            Init_RewardedVideo_Debug();
        }

        void Init_RewardedVideo_Debug()
        {
            rewarded_ShowAdRequest = new ShowAdRequest();

            rewarded_ShowAdRequest.OnDisplayed += (adUnitId) => Debug.Log("Displayed Rewarded Ad :: Ad Unit ID = " + adUnitId);
            rewarded_ShowAdRequest.OnClicked += (adUnitId) => Debug.Log("Clicked Rewarded Ad :: Ad Unit ID = " + adUnitId);
            rewarded_ShowAdRequest.OnHidden += (adUnitId) => Debug.Log("Closed Rewarded Ad :: Ad Unit ID = " + adUnitId);
            rewarded_ShowAdRequest.OnFailedToDisplay += (adUnitId, error) => Debug.LogError("Failed To Display Rewarded Ad :: Error = " + error + " :: Ad Unit ID = " + adUnitId);
            rewarded_ShowAdRequest.OnReceivedReward += (adUnitId, reward) => Debug.Log("Received Reward :: Reward = " + reward + " :: Ad Unit ID = " + adUnitId);
        }

        public void ShowRewardedVideo_Debug(int level = -1)
        {
            if (rewarded_ShowAdRequest != null)
            {
                rewarded_ShowAdRequest.SetLevel(level);
                RewardedAd.Show(rewarded_ShowAdRequest);
            }
            else
            {
                Debug.LogError("Rewarded Ad Not Initialized");
            }
        }

        void Init_InterstitialAd_Debug()
        {
            interstitial_ShowAdRequest = new ShowAdRequest();

            interstitial_ShowAdRequest.OnDisplayed += (adUnitId) => Debug.Log("Displayed Interstitial Ad :: Ad Unit ID = " + adUnitId);
            interstitial_ShowAdRequest.OnClicked += (adUnitId) => Debug.Log("Clicked Interstitial Ad :: Ad Unit ID = " + adUnitId);
            interstitial_ShowAdRequest.OnHidden += (adUnitId) => Debug.Log("Closed Interstitial Ad :: Ad Unit ID = " + adUnitId);
            interstitial_ShowAdRequest.OnFailedToDisplay += (adUnitId, error) => Debug.LogError("Failed To Display Interstitial Ad :: Error = " + error + " :: Ad Unit ID = " + adUnitId);
            //interstitial_ShowAdRequest.OnReceivedReward += (adUnitId, reward) => Debug.Log("Received Reward :: Reward = " + reward + " :: Ad Unit ID = " + adUnitId);
        }

        public void ShowInterstitialAd_Debug(int level = -1)
        {
            if (interstitial_ShowAdRequest != null)
            {
                interstitial_ShowAdRequest.SetLevel(level);
                Interstitial.Show(interstitial_ShowAdRequest);
            }
            else
            {
                Debug.LogError("Interstitial Ad Not Initialized");
            }
        }

        void Init_BannerAd_Debug()
        {
            banner_ShowAdRequest = new ShowAdRequest();

            banner_ShowAdRequest.OnDisplayed += (adUnitId) => Debug.Log("Displayed Banner Ad :: Ad Unit ID = " + adUnitId);
            banner_ShowAdRequest.OnClicked += (adUnitId) => Debug.Log("Clicked Banner Ad :: Ad Unit ID = " + adUnitId);
            banner_ShowAdRequest.OnHidden += (adUnitId) => Debug.Log("Closed Banner Ad :: Ad Unit ID = " + adUnitId);
            banner_ShowAdRequest.OnFailedToDisplay += (adUnitId, error) => Debug.LogError("Failed To Display Banner Ad :: Error = " + error + " :: Ad Unit ID = " + adUnitId);
            //interstitial_ShowAdRequest.OnReceivedReward += (adUnitId, reward) => Debug.Log("Received Reward :: Reward = " + reward + " :: Ad Unit ID = " + adUnitId);
        }

        public void ShowBannerAd_Debug(int level = -1)
        {
            if (banner_ShowAdRequest != null)
            {
                banner_ShowAdRequest.SetLevel(level);
                Banner.Show(banner_ShowAdRequest);
            }
            else
            {
                Debug.LogError("Banner Ad Not Initialized");
            }
        }
        #endregion


        #region Initialization

        ////Auto Initialization:
        //[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        //void Initialize()
        //{
        //    //Pre Initialization

        //    //Initialization
        //    LionKit.OnInitialized += () =>
        //    {
        //        Facebook_Init();

        //        Adjust_Init();
        //    };
        //}

        ////Auto Initialization ; Called After Awake():
        //[RuntimeInitializeOnLoadMethod]
        //void Initialize_Delayed()
        //{
        //    GameAnalytics_Init();
        //}

        //void Facebook_Init()
        //{
        //    //Set Instance in .BeforeSceneLoad with script execution order coming before SDKManager
        //    FacebookManager.Instance.Initialization();
        //}

        //void Adjust_Init()
        //{
        //    //Set Instance in .BeforeSceneLoad with script execution order coming before SDKManager
        //    AdjustSDKManager.Instance.Initialization();
        //}

        //void GameAnalytics_Init()
        //{
        //    GameAnalytics.Initialize();
        //} 

        #endregion


        #region Ad Callbacks

        public static void ShowRewardedVideo(bool useCallbacks = true, int level = -1,
            System.Action<string> OnDisplayed = null, System.Action<string> OnClicked = null,
            System.Action<string> OnHidden = null, System.Action<string, int> OnFailedToDisplay = null,
            System.Action<string, MaxSdkBase.Reward> OnReceivedReward = null)
        {
            ShowAdRequest showAdRequest = null;
            if (useCallbacks)
            {
                showAdRequest = new ShowAdRequest();

                showAdRequest.OnDisplayed += OnDisplayed;
                showAdRequest.OnClicked += OnClicked;
                showAdRequest.OnHidden += OnHidden;
                showAdRequest.OnFailedToDisplay += OnFailedToDisplay;
                showAdRequest.OnReceivedReward += OnReceivedReward;

                showAdRequest.SetLevel(level);
            }

            LionStudios.Runtime.Sdks.AppLovin.WhenInitialized(() => RewardedAd.Show(showAdRequest));
        }

        public static void ShowInterstitialAd(bool useCallbacks = false, int level = -1,
            System.Action<string> OnDisplayed = null, System.Action<string> OnClicked = null,
            System.Action<string> OnHidden = null, System.Action<string, int> OnFailedToDisplay = null,
            System.Action<string, MaxSdkBase.Reward> OnReceivedReward = null)
        {
            ShowAdRequest showAdRequest = null;
            if (useCallbacks)
            {
                showAdRequest = new ShowAdRequest();

                showAdRequest.OnDisplayed += OnDisplayed;
                showAdRequest.OnClicked += OnClicked;
                showAdRequest.OnHidden += OnHidden;
                showAdRequest.OnFailedToDisplay += OnFailedToDisplay;
                showAdRequest.OnReceivedReward += OnReceivedReward;

                showAdRequest.SetLevel(level);
            }

            LionStudios.Runtime.Sdks.AppLovin.WhenInitialized(() => Interstitial.Show(showAdRequest));
        }

        public static void ShowBannerAd(bool useCallbacks = false, int level = -1,
            System.Action<string> OnDisplayed = null, System.Action<string> OnClicked = null,
            System.Action<string> OnHidden = null, System.Action<string, int> OnFailedToDisplay = null,
            System.Action<string, MaxSdkBase.Reward> OnReceivedReward = null)
        {
            ShowAdRequest showAdRequest = null;
            if (useCallbacks)
            {
                showAdRequest = new ShowAdRequest();

                showAdRequest.OnDisplayed += OnDisplayed;
                showAdRequest.OnClicked += OnClicked;
                showAdRequest.OnHidden += OnHidden;
                showAdRequest.OnFailedToDisplay += OnFailedToDisplay;
                showAdRequest.OnReceivedReward += OnReceivedReward;

                showAdRequest.SetLevel(level);
            }

            LionStudios.Runtime.Sdks.AppLovin.WhenInitialized(() => Banner.Show(showAdRequest));
        } 

        #endregion


        public static void ShowMaxMediationDebugger()
        {
            MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) =>
            {
                // Show Mediation Debugger
                MaxSdk.ShowMediationDebugger();
            };
        }
    }
}
