﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Dev_Nirjhor
{
    public class IntegrationCheck : MonoBehaviour
    {
        public static int currentLevel = 1;

        public Button nextLevel, retryLevel;

        public Button testButton;
        int buttonClicked = 0;

        // Start is called before the first frame update
        void Start()
        {
            nextLevel.onClick.AddListener(() => GoNext());
            retryLevel.onClick.AddListener(() => Retry());

            testButton.onClick.AddListener(() => buttonClicked++);
            GameAnalytics.Initialize();
            Debug.Log($"Started: level{currentLevel.ToString("00")}");
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "world01", $"level{currentLevel.ToString("00")}");
            AdjustSDKManager.Instance.InvokePreConfigEvent(AdjustEventTrack.test_event_delete);

            StartCoroutine(GamePlay());
        }

        IEnumerator GamePlay()
        {
            yield return new WaitForSeconds(5f);

            if (buttonClicked > 5)
            {
                LevelComplete();
            }
            else
            {
                LevelFailed();
            }
        }

        public void GoNext()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public void Retry()
        {
            LevelRestart();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public void LevelComplete()
        {
            Debug.Log($"Complete: level{currentLevel.ToString("00")}");
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "world01", $"level{currentLevel.ToString("00")}");
            
            currentLevel++;

            nextLevel.gameObject.SetActive(true);
        }

        public void LevelFailed()
        {
            Debug.Log($"Failed: level{currentLevel.ToString("00")}");
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "world01", $"level{currentLevel.ToString("00")}");

            retryLevel.gameObject.SetActive(true);
        }

        void LevelRestart()
        {
            Debug.Log($"Restart: level{currentLevel.ToString("00")}");
            GameAnalytics.NewDesignEvent("Restart:"+ "world01:"+ $"level{currentLevel.ToString("00")}");
        }
    } 
}
